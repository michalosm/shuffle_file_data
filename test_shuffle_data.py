import unittest
import shuffle_data as s
import os

class TestShuffle(unittest.TestCase):

    def get_lines(self,file_name):
        lines = []
        with open(file_name) as file:
            line = file.readline()
            lines.append(line)
            while line:
                line = file.readline()
                if line!='':
                    lines.append(line)
        return lines

    def get_number_of_lines(self,file_name):
        counter = 0
        with open(file_name) as file:
            line = file.readline()
            while line:
                line = file.readline()
                counter+=1
        return counter

    def test_first_lines(self):
        s.first_lines('1.txt',3)
        file_name = 'results-first-lines3.txt'
        self.assertEqual(3,self.get_number_of_lines(file_name),"Should generate 3 first lines")
        expected_lines = ['Kochana Gromadko!\n', 'Temat GF17 usunięto!\n', 'W tej sprawie wysłałam już zapytanie do Moderatora.'] 
        self.assertEqual(expected_lines,self.get_lines(file_name),"Should generate expected lines") 
        os.remove(file_name)

    def test_last_lines(self):
        s.last_lines('1.txt',3)
        file_name = 'results-last_lines3.txt'
        self.assertEqual(3,self.get_number_of_lines(file_name),"Should generate 3 first lines")
        expected_lines = ['Traktuja ją troche po macoszemu.\n', 'Z drugiej strony, chyba wiedziala na czym zabawa polega, i jej nadąsana mina podczas punktowania w ostatnim odcinku byla komiczna.\n', 'Jak mala dziewczynka, ktorej mama nie chce kupic lizaka.\n']
        self.assertEqual(expected_lines,self.get_lines(file_name),"Should generate expected lines") 
        os.remove(file_name)    
    
    def test_random_lines(self):
        s.get_random_lines('1.txt',1,True)
        file_name = 'results-percentages1.txt'
        self.assertEqual(306,self.get_number_of_lines(file_name),"Should generate 306 lines")
        os.remove(file_name)


if __name__ == '__main__':
    unittest.main()


