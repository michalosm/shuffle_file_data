import sys                                                                                                                             
import os
import subprocess
import random

keys = ["percentages=","first_lines=","last_lines=","file_name=","lines=","--help"]     



def process_parameters():
    file_name = ''
    counter_first_lines = ''
    counter_last_lines = ''
    percentage = ''
    is_percentage = False
    for i in range(1,len(sys.argv)):                                                                                                       
        for key in keys:                                                                                                
            if sys.argv[i].find(key) == 0 and key=='--help':
                with open('Readme.rd','r') as f:
                    line = f.readline()
                    while line:
                        print(line)
                        line = f.readline()
                return 1

    for i in range(1,len(sys.argv)):                                                                                                       
        for key in keys:                                                                                                
            if sys.argv[i].find(key) == 0:
                value = sys.argv[i][len(key):]
                if key=='file_name=':
                    file_name = value
                if key=='last_lines=':
                    counter_last_lines = value    
                if key=='first_lines=':                                                             
                    counter_first_lines = int(value)
                if key=='percentages=':
                    percentage = int(value)
                    is_percentage = True     
                if key=='percentages=':
                    percentage = int(value)    
                if key=='lines=':
                    is_percentage = False
                    percentage =int(value)
                  

    if  counter_first_lines !='' and file_name!='':
        first_lines(file_name,counter_first_lines)
    if  counter_last_lines !='' and file_name!='':
        last_lines(file_name,counter_last_lines)
    if  percentage !='' and file_name!='':
        get_random_lines(file_name,percentage,is_percentage)  
           

def first_lines(file_name,count):
    name = file_name.split(".")[0]
    output_file_name = f'results-first-lines{count}.txt'
    output = open(output_file_name,'w')
    counter = 0
    line = 'First Line'
    with open(file_name,'r') as f:
        while (line):
            line = f.readline()
            counter+=1
            if counter!=count:
                output.write(line)
            else:
                output.write(line.strip())
                break
    output.close()

def last_lines(file_name,count):
    output_file_name = f'results-last_lines{count}.txt'
    output = open(output_file_name,'w')
    lines = subprocess.check_output(f'tail -{count} {file_name}',shell=True).decode('utf-8')
    output.write(lines)
    output.close()

def get_random_lines(file_name,count,percentage=False):
    if percentage==True:
        output_file_name = f'results-percentages{count}.txt'
    else:
        output_file_name = f'results-lines{count}.txt'
    output = open(output_file_name,'w')
    with open(file_name,'r') as f:
        lines = f.readlines()
        if percentage:
            results = random.sample(lines,count*len(lines)//100)
        else:
            results = random.sample(lines,count)
        for result in results:
            output.write(result)
    output.close() 

if __name__ == "__main__":
    process_parameters()

