import sys

def get_number_of_lines(file_name):
        counter = 0
        with open(file_name) as file:
            line = file.readline()
            while line:
                line = file.readline()
                counter+=1
        return counter

def get_name_of_file(name,lines,number):
    add_0 = len(str(lines))-len(str(number))
    file_name = f"{name}-{add_0*'0'}{number}.txt"
    return file_name

def seperate_file(file_name):
    lines = get_number_of_lines(file_name)
    name = file_name.split(".")[0]
    with open(file_name) as f:
        line = 'First line'
        number = 0
        while line:
            line = f.readline()
            if line:
                number+=1
                with open(get_name_of_file(name,lines,number),'w') as output:
                    output.write(line)

if __name__=='__main__':
   seperate_file(sys.argv[1])




