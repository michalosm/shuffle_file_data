import unittest
import seperate_file as s


class TestShuffle(unittest.TestCase):

    def test_get_name_file(self):
        self.assertEqual('1-005.txt',s.get_name_of_file('1',100,5),'Test name generation for number file')
        self.assertEqual('1-099.txt',s.get_name_of_file('1',100,99),'Test name generation for number file')


if __name__ == '__main__':
    unittest.main()        